# Calculadora de comparação de preços de combustíveis Álcool & Gasolina ⛽.


              É usado uma fórmula bem simples.

       1° Dividimos o valor do litro do álcool pelo da gasolina. 
 
        2° Quando o resultado é menor que 0.7, a recomendação é abastecer com álcool, Se maior, a recomendação é escolher a gasolina.
 
        Exemplo: se o álcool custa R$ 4,92, o resultado da divisão do primeiro pelo segundo é 0,67, menor que 0,7. Portanto, é mais vantajoso abastecer com álcool.
 

